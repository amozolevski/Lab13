package FacebookTest;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.By.xpath;

public class FacebookTest {

    WebDriver driver;
    WebDriverWait wait;

    @BeforeClass
    void before() throws MalformedURLException{
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub/"),
                                    DesiredCapabilities.chrome());

        driver.get("https://facebook.com");

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        wait = new WebDriverWait(driver, 10);
    }

    @Test()
    void signInTestCase(){
        driver.findElement(By.id("email")).sendKeys("litstest@i.ua");
        driver.findElement(By.id("pass")).sendKeys("linux-32");
        driver.findElement(By.id("loginbutton")).click();

        By profileTitle = xpath(".//a[contains(@title, 'Profile')]");
        wait.until(ExpectedConditions.presenceOfElementLocated(profileTitle));
        WebElement profile = driver.findElement(profileTitle);
//        WebElement feedNews = driver.findElement(By.xpath(".//div[contains(@id,'feed')]"));
        Assert.assertTrue(profile.isDisplayed(), "can't see profile icon");

        driver.findElement(xpath(".//body")).sendKeys(Keys.ESCAPE);
    }

    @Test(priority = 1)
    void addFriendTestCase(){

        driver.findElement(By.id("findFriendsNav")).click();

//        try{
//            wait.until((WebDriver webdriver) -> webdriver.findElement(By.id("fbSearchResultsBox")).isDisplayed());
//        } catch (TimeoutException e){
//            driver.navigate().refresh();
//        }

        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("fbSearchResultsBox")));

        List<WebElement> list = driver.findElements(xpath(".//*[@id='fbSearchResultsBox']//ul/li//button[1]"));
        WebElement secondFriend = list.get(1);
        secondFriend.click();

        By confirmButton = xpath(".//button[contains(text(), 'Confirm')]");
        try{
            wait.until(ExpectedConditions.presenceOfElementLocated(confirmButton));
            driver.findElement(confirmButton).click();
        } catch (TimeoutException e){

        }

        String addButtonColor = secondFriend.getCssValue("background-color");
        Assert.assertNotEquals("#4267b2", addButtonColor, "U didn't send add friend request");
    }

    @Test(priority = 2)
    void signOutTestCase(){

        driver.findElement(By.id("userNavigationLabel")).click();

        By local = xpath(".//ul[@role='menu']//li[last()]");
        wait.until(ExpectedConditions.presenceOfElementLocated(local));
        driver.findElement(local).click();
        Assert.assertTrue(driver.findElement(By.id("email")).isDisplayed(), "can't see email input");
    }

    @DataProvider(name = "emailTest")
    public Object[][] createEmailData(){
        return new Object[][]{
//                            {"first"},
                            {"second@go.ua"},
//                            {"third"},
                            {"fourth@go.ua"},
//                            {"fifth"},
//                            {"sixth"},
                            {"seventh@go.ua"},
                            {"eighth@go.ua"},
                            {"nineth@go.ua"}
//                            {"tenth"}
        };
    }

    @Test(priority = 3, dataProvider = "emailTest")
    void verifyEmailTestCase(String e) {
        WebElement email = driver.findElement(By.name("reg_email__"));
        email.sendKeys(e);
        email.sendKeys(Keys.RETURN);
        email.clear();

        try{
            Thread.sleep(1000);
            WebElement element = driver.findElement(xpath("//input[contains(@aria-label,'email')]"));
            Assert.assertNotEquals(element.getAttribute("aria-invalid"), "true", "email is incorrect");
        } catch (Exception ex){
        }
    }

    @AfterClass()
    void after(){
        driver.quit();
    }
}