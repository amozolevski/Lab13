package TemplateMonster;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TemplateTest {
    WebDriver driver;
    WebDriverWait wait;

    @BeforeClass
    void init() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub/"),
                                    DesiredCapabilities.chrome());
        driver.get("https://www.templatemonster.com/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 10);
    }

    @AfterMethod
    void escClick(){
        driver.findElement(By.xpath(".//body")).sendKeys(Keys.ESCAPE);
    }

    @Test
    void waitForCookieTestCase(){
        String cookieName = "aff";
        wait.until((WebDriver driver) -> driver.manage().getCookieNamed(cookieName));
        Assert.assertEquals(driver.manage().getCookieNamed(cookieName).getValue(), "TM", "cookie " + cookieName + " is not present");
    }

    private void selectLanguage(String language){
        String langId = "menu-" + language + "-locale";
        driver.findElement(By.xpath("//*[contains(@class,'language-pick')]")).click();
        wait.until((WebDriver langDriver) -> langDriver.findElement(By.id(langId)).isDisplayed());
        driver.findElement(By.id(langId)).click();
    }

    @DataProvider(name = "languages Locale")
    Object[][] initLanguageLocale(){
        return new String[][]{
                {"UA"},
                {"PL"},
                {"FR"},
                {"CZ"}
        };
    }

    @Test(priority = 1, dataProvider = "languages Locale")
    void languageLocaleTestCase(String language){
        String lang = language;
        selectLanguage(lang);

        String expectUrl = "https://www.templatemonster.com/" + lang.toLowerCase() + "/";
        wait.until((WebDriver driverURL) -> driverURL.getCurrentUrl().equals(expectUrl));
        Assert.assertTrue(driver.getCurrentUrl().equals(expectUrl), "it isn't expected URL");
    }

    @AfterClass
    void after(){
        driver.quit();
    }
}